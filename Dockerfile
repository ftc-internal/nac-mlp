FROM nginx:1.17.5
LABEL maintainer="MrFlutters (https://github.com/MrFlutters)"

ENV S3_URL="https://s3.nac.fluttershub.com:8787" \
    S3_BUCKET="nac-mlp" \
    HORSECRAFT_GNU_VERSION="v1.0.0" \
    HORSECRAFT_NT_VERSION="v1.0.0" \
    HORSECRAFT_MODS_VERSION="v1.0.0"

ARG S3_AccessKey
ARG S3_SecretKey

COPY build/nginx/nginx.conf /etc/nginx/nginx.conf
COPY build/nginx/web.conf /etc/nginx/conf.d/web.conf
RUN rm -Rf /usr/share/nginx/html/ \
    && rm /etc/nginx/conf.d/default.conf \
    && mkdir /usr/share/nginx/html/

EXPOSE 80

COPY set-date.sh .
RUN apt-get update \ 
    && apt-get -qq -o=Dpkg::Use-Pty=0 install jq wget > /dev/null \
    && bash set-date.sh \ 
    && apt-get -qq -o=Dpkg::Use-Pty=0 remove jq wget > /dev/null \
    && apt-get -qq -o=Dpkg::Use-Pty=0 clean autoclean > /dev/null \
    && apt-get -qq -o=Dpkg::Use-Pty=0 autoremove > /dev/null \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/

# Lighter files that are more often changed
COPY ["src/packer", "/usr/share/nginx/html/"]
COPY ["src/PackUpdaterWin", "/usr/share/nginx/html/PackUpdaterWin"]
COPY ["src/version", "src/versionw", "/usr/share/nginx/html/"]
COPY ["src/list", "src/listOld", "src/listserver", "/usr/share/nginx/html/"]

# Update file permissions
RUN chmod -R 755 /usr/share/nginx/html/*

CMD [ "nginx", "-g", "daemon off;" ]