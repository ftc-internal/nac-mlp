#!/bin/bash

# Make Directory
mkdir -p /temp/bin

# Download minio Client
echo "[Minio-Client]: Downloading minio client."
wget https://dl.min.io/client/mc/release/linux-amd64/mc -qO /temp/bin/mc
echo "[Minio-Client]: Done."
chmod +x ./temp/bin/mc

# Configure minio Client
./temp/bin/mc config host add nac-s3 $S3_URL $S3_AccessKey $S3_SecretKey --api S3v4

# Download Files
echo "[HORSECRAFT-GNU]: Downloading HORSECRAFT-GNU.zip"
./temp/bin/mc cp --quiet nac-s3/$S3_BUCKET/$HORSECRAFT_GNU_VERSION/HORSECRAFT-GNU.zip /usr/share/nginx/html/HORSECRAFT-GNU.zip
echo "[HORSECRAFT-GNU]: Done."

echo "[HORSECRAFT-NT]: Downloading HORSECRAFT-NT.zip"
./temp/bin/mc cp --quiet nac-s3/$S3_BUCKET/$HORSECRAFT_NT_VERSION/HORSECRAFT-NT.zip /usr/share/nginx/html/HORSECRAFT-NT.zip
echo "[HORSECRAFT-NT]: Done."

echo "[MODS-7z]: Downloading mods.7z"
./temp/bin/mc cp --quiet nac-s3/$S3_BUCKET/$HORSECRAFT_MODS_VERSION/mods.7z /usr/share/nginx/html/mods.7z
echo "[MODS-7z]: Done."

# Grab the lastModified date from Minio
HORSECRAFT_GNU_DATE=$(./temp/bin/mc stat nac-s3/${S3_BUCKET}/${HORSECRAFT_GNU_VERSION}/HORSECRAFT-GNU.zip --json | jq --raw-output .lastModified)
HORSECRAFT_NT_DATE=$(./temp/bin/mc stat nac-s3/${S3_BUCKET}/${HORSECRAFT_GNU_VERSION}/HORSECRAFT-NT.zip --json | jq --raw-output .lastModified)
HORSECRAFT_MODS_DATE=$(./temp/bin/mc stat nac-s3/${S3_BUCKET}/${HORSECRAFT_GNU_VERSION}/mods.7z --json | jq --raw-output .lastModified)

# Set the Dates
touch -d"$HORSECRAFT_GNU_DATE" /usr/share/nginx/html/HORSECRAFT-GNU.zip
touch -d"$HORSECRAFT_NT_DATE" /usr/share/nginx/html/HORSECRAFT-NT.zip
touch -d"$HORSECRAFT_MODS_DATE" /usr/share/nginx/html/mods.7z

# Clean up any possible leftovers
rm -Rf /temp

# Echo result
echo "Set HORSECRAFT-GNU.zip date to $HORSECRAFT_GNU_DATE"
echo "Set HORSECRAFT-NT.zip date to $HORSECRAFT_NT_DATE"
echo "Set mods.7z date to $HORSECRAFT_MODS_DATE"